var gulp = require('gulp');
var gutil = require('gulp-util');
var sass = require('gulp-ruby-sass');
var autoprefix = require('gulp-autoprefixer');
var minifyCSS = require('gulp-minify-css');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var es = require('event-stream');

// Assets dir
var assetsDir = 'app/assets';

// Which directory should Sass compile to?
var targetCSSDir = 'public/css';

// Which directory should Javascript compile to?
var targetJSDir = 'public/js';

// Handle JavaScript compilation
gulp.task('js', function () {

    var vendorJS = [
        'app/assets/components/jquery/jquery.js',
        'app/assets/components/underscore/underscore.js',
        'app/assets/components/backbone/backbone.js',
        'app/assets/components/bootstrap/bootstrap.js',
         assetsDir + '/js/*.js'
    ];

    return gulp.src(vendorJS)
        .pipe(uglify().on('error', gutil.log))
        .pipe(concat('min.js'))
        .pipe(gulp.dest(targetJSDir));
});

// Compile Sass, autoprefix CSS3, merge with vendor css files
// and concatenate to min.css
gulp.task('css', function(){
    var vendorFiles = gulp.src(assetsDir + '/components/**/*.css');
    var appFiles = gulp.src(assetsDir + '/sass/**/*.sass')
        .pipe(sass({'sourcemap=none': true, style: 'compressed' }).on('error', gutil.log));

    return es.concat(vendorFiles, appFiles)
        .pipe(autoprefix('last 5 version'))
        .pipe(minifyCSS({'keepSpecialComments': 0}))
        .pipe(concat('min.css'))
        .pipe(gulp.dest(targetCSSDir));
});

// Keep an eye on Sass and JS for changes...
gulp.task('watch', function () {
    gulp.watch(assetsDir + '/sass/*.sass', ['css']);
    gulp.watch(assetsDir + '/js/*.js', ['js']);
});

// What tasks does running gulp trigger?
gulp.task('default', ['css', 'js', 'watch']);
