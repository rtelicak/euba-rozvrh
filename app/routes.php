<?php

Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);


Route::get('/svc/department', 'AjaxController@getDepartments');
Route::get('/svc/group', 'AjaxController@getGroups');

Route::group(['prefix' => 'svc'], function(){
    Route::get('getVersion', 'ServiceController@getVersion');
    Route::get('getFaculty', 'ServiceController@getFaculty');
    Route::get('getLesson', 'ServiceController@getLesson');
    Route::get('getInstitute', 'ServiceController@getInstitute');
    Route::get('getGroup', 'ServiceController@getGroup');
    Route::get('getRoom', 'ServiceController@getRoom');
    Route::get('getDepartment', 'ServiceController@getDepartment');
    Route::get('getSubject', 'ServiceController@getSubject');
    Route::get('getTeacher', 'ServiceController@getTeacher');
    Route::get('getLecture', 'ServiceController@getLecture');
    Route::get('getDay', 'ServiceController@getDay');
});

