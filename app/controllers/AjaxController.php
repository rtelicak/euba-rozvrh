<?php

use Schedule\Repositories\SearchRepository;

class AjaxController extends \BaseController {

    protected $searchRepository;

    function __construct(SearchRepository $searchRepository) {
        $this->searchRepository = $searchRepository;
    }

    public function getDepartments() {
        return $this->searchRepository->getDepartmentsByFacultyAndYear(Input::get('faculty_id'), Input::get('year_id'));
    }

    public function getGroups() {
        return $this->searchRepository->getGroupsByDepartment(Input::get('department_id'));
    }

}
