<?php

use Schedule\Repositories\SearchRepository;
use Schedule\Repositories\PresenterRepository as PresenterRepository;
use Schedule\Localizer;

class HomeController extends BaseController {

    protected $searchRepository;

    function __construct(SearchRepository $searchRepository) {
        $this->searchRepository = $searchRepository;
    }

	public function index() {

        $table = $departments = $groups = $lessons = $nextLesson = [];

        if (count(Input::all())) {
            extract(Input::all());

            $departments = $this->searchRepository->getListOfDepartmentsByFacultyAndYear($faculty, $year);
            $groups = $this->searchRepository->getListOfGroupsByDepartment($department);
            $lessons = $this->searchRepository->getLessons($group);
            $presenterRepository = new PresenterRepository($lessons);
            $table = $presenterRepository->getTableMarkup();
            $nextLesson = $presenterRepository->getNextLesson();
            $currentLesson = $presenterRepository->getCurrentLesson();
        }

		return View::make('home', array(
            'years' => Year::getList(),
            'faculties' => Faculty::lists('nazov', 'id'),
            'departments' => $departments,
            'groups' => $groups,
            'table' => $table,
            'nextLesson' => $nextLesson,
            'dt' => Localizer::getFormattedDate(),
            'currentLesson' => isset($currentLesson) ? $currentLesson : null
        ));
	}

}
