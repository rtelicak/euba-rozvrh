<?php

class ServiceController extends \BaseController {

    public function getVersion() {
        return Version::first();
    }
    public function getFaculty() {
        return Faculty::all();
    }
    // katedra
    public function getInstitute() {
        return Institute::all();
    }
    public function getGroup() {
        return Group::all();
    }
    public function getRoom() {
        return Room::all();
    }
    public function getDepartment() {
        return Department::all();
    }
    public function getSubject() {
        return Subject::all();
    }
    public function getTeacher() {
        return Teacher::all();
    }
    public function getLecture() {
        return Lesson::all();
    }
    public function getDay() {
        return [
            ["id_den" => "1","den" => "Pondelok"],
            ["id_den" => "2","den" => "Utorok"],
            ["id_den" => "3","den" => "Streda"],
            ["id_den" => "4","den" => "Štvrtok"],
            ["id_den" => "5","den" => "Piatok"]];
    }
    public function getLesson() {
        return [
            ["id_hodina" => "1","cislo" => "1","cas_od" => "07:30:00","cas_do" => "09:00:00"],
            ["id_hodina" => "2","cislo" => "2","cas_od" => "09:15:00","cas_do" => "10:45:00"],
            ["id_hodina" => "3","cislo" => "3","cas_od" => "11:00:00","cas_do" => "12:30:00"],
            ["id_hodina" => "4","cislo" => "4","cas_od" => "13:30:00","cas_do" => "15:00:00"],
            ["id_hodina" => "5","cislo" => "5","cas_od" => "15:15:00","cas_do" => "16:45:00"],
            ["id_hodina" => "6","cislo" => "6","cas_od" => "17:00:00","cas_do" => "18:30:00"],
            ["id_hodina" => "7","cislo" => "7","cas_od" => "18:35:00","cas_do" => "20:05:00"]];
    }

}
