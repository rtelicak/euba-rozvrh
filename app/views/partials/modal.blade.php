<div id="detail-modal" class="modal fade">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title"></div>
      </div>
      <div class="modal-body">
            <div class="teacher">
                <label>Vyučujúci: </label>
                <p></p>
            </div>
            <div class="room">
                <label>Miestnosť: </label>
                <p></p>
            </div>
            <div class="type">
                <label>Typ: </label>
                <p></p>
            </div>
            <div class="kind">
                <label>Druh: </label>
                <p></p>
            </div>
            <div class="day">
                <label>Deň: </label>
                <p></p>
            </div>
            <div class="duration">
                <label>Čas: </label>
                <p></p>
            </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Ok, cool</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
