<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Fero Taraba">
    <meta name="description" content="Prehľadný a aktuálny (momentálne zimný semester 2014/2015) rozvrh hodín pre študentov EUBA. Goodbye Lookin!">
    <meta name="keywords" content="Lookin, EUBA, rozvrh, Ekonomicka univerzia v Bratislave">
    <meta name="robots" content="ALL,FOLLOW">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/min.css">
    <link rel="SHORTCUT ICON" href="favicon.ico">
	<title>Euba rozvrh</title>
</head>
<body>

    <?php $searchCriteria = Request::query() ?>


    <div class="container-fluid header">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 logo"></div>
                <div class="col-sm-5"> </div>
                <div class="col-sm-3 dateTime">{{ $dt }}</div>
            </div>
        </div>
    </div>
    <div class="container-fluid searchForm">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    @if (empty($nextLesson))
                        <h1>Vyber si svoj ročník, fakultu, odbor a krúžok a je to!</h1>
                    @endif

                    {{ Form::model($searchCriteria, ['method' => 'GET', 'class' => 'form-inline' ]) }}

                        <div class="row">
                            <div class="col-sm-12">
                                <!-- Year form select -->
                                <div class="form-group">
                                    {{ Form::label('year', 'Rok', array('class' => 'control-label first')) }}
                                    {{ Form::select('year', ['' => 'Vyber'] + $years, null ,array('class' => 'form-control')) }}
                                </div>

                                <!-- Faculty form select -->
                                <div class="form-group">
                                    {{ Form::label('faculty', 'Fakulta', array('class' => 'control-label')) }}
                                    {{ Form::select('faculty', ['' =>'Vyber'] + $faculties, null ,array('class' => 'form-control')) }}
                                </div>

                                <!-- Department form select -->
                                <div class="form-group">
                                    {{ Form::label('department', 'Odbor', array('class' => 'control-label')) }}
                                    {{ Form::select('department', ['' =>'Vyber'] + $departments, null ,array('class' => 'form-control')) }}
                                </div>

                                <!-- Group form select -->
                                <div class="form-group">
                                    {{ Form::label('group', 'Krúžok', array('class' => 'control-label')) }}
                                    {{ Form::select('group', ['' =>'Vyber'] + $groups, null ,array('class' => 'form-control')) }}
                                </div>

                                    {{ Form::submit('Go!', ['class' => 'btn btn-default']) }}
                            </div>

                        </div>

                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
    @if (! empty($nextLesson))
    <div class="container">
        <div class="row">
            <div class="col-sm-6 currentLesson">
                <span class="lessonLabel">Aktuálna hodina: </span>
                @if (! is_null($currentLesson))
                    <span class="currentLesson">
                        <span class="lessonTitle">{{ $currentLesson->subject->nazov }}</span><span class="delimiter"></span>
                        <span class="lessonDuration">{{ $currentLesson->duration() }}</span><span class="delimiter"></span>
                        <span class="room">{{ $currentLesson->room->nazov }}</span>
                    </span>
                @else
                    <span>N/A</span>
                @endif
            </div>
            <div class="col-sm-6 nextLessonBox">
                <span class="lessonLabel">Nasleduje: </span>
                @if (is_array($nextLesson) && ! empty($nextLesson))
                    @foreach ($nextLesson as $lesson)
                        <span class="nextLesson">
                            <span class="lessonTitle">{{ $lesson->subject->nazov }}</span><span class="delimiter"></span>
                            <span class="lessonDuration">{{ $lesson->duration() }}</span><span class="delimiter"></span>
                            <span class="room">{{ $lesson->room->nazov }}</span>
                        </span>
                    @endforeach
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 table-container">
                @if (count($table))
                    {{ $table }}
                @endif

            </div>
        </div>
        <div class="col-sm-12 legend clearfix">
            <span class="legendTitle">Rozumej: </span>
            <div class="boxes">
                <span class="lessonSample lecture compulsory">Povinny predmet prednaska</span>
                <span class="lessonSample excersise compulsory">Povinny predmet cvicenie</span>
            </div>
            <div class="boxes">
                <span class="lessonSample lecture optional">PVP/VP prednaska</span>
                <span class="lessonSample excersise optional">PVP/VP cvicenie</span>
            </div>
        </div>
    </div>
    @endif

    <div class="container-fluid">
        <div class="container footer">
            Last updated: 29.10. 2014 | Prevádzkovateľ stránky ručí za nič.
        </div>
    </div>

    @include('partials.modal')

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-55152269-1', 'auto');
  ga('send', 'pageview');

</script>

<script src="js/min.js"></script>
</body>
</html>
