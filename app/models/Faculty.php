<?php

class Faculty extends \Eloquent {

	protected $fillable = [];

    protected $table = 'fakulta';

    public function getNazovAttribute($nazov) {
         return str_replace('_', ' ', $nazov);
     }

}
