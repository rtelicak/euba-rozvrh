<?php

class Year extends \Eloquent {

	protected $fillable = [];

    public static function getList() {
        return [
            1 => '1. Ročník',
            2 => '2. Ročník',
            3 => '3. Ročník',
            4 => '4. Ročník',
            5 => '5. Ročník'
        ];
    }

}
