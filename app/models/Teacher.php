<?php

class Teacher extends \Eloquent {
	protected $fillable = [];

    protected $table = 'ucitel';

    protected $primaryKey = 'id_ucitel';

    public function fullName() {

        return $this->titul ?
            $this->titul .'. '. $this->meno . ' '. $this->priezvisko :
            $this->meno . ' '. $this->priezvisko;
    }
}
