<?php

class Room extends \Eloquent {
	protected $fillable = [];

    protected $table = 'miestnost';

    protected $primaryKey = 'id_miestnost';

    public function getNazovAttribute($nazov) {
        return str_replace('_', ' ', $nazov);
    }
}
