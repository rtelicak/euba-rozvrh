<?php

class Subject extends \Eloquent {
	protected $fillable = [];

    protected $table = 'predmet';

    public function getNazovAttribute($nazov) {
        return str_replace('_', ' ', $nazov);
    }
}
