<?php

class Lesson extends \Eloquent {
	protected $fillable = [];

    protected $table = 'vyuka';

    public function hour() {
        return $this->belongsTo('Hour', 'id_hodina');
    }

    public function subject() {
        return $this->belongsTo('Subject', 'id_predmet');
    }

    public function teacher() {
        return $this->belongsTo('Teacher', 'id_ucitel');
    }

    public function room() {
        return $this->belongsTo('Room', 'id_miestnost');
    }

    public function isItNow($day, $lesson) {
        return $this->id_den == $day && $this->id_hodina == $lesson;
    }

    public function type() {
        return $this->prednaska ? 'Prednáška' : 'Cvičenie';
    }

    public function duration() {
        return substr($this->hour->cas_od, 0, 5)  .' - '. substr($this->hour->cas_do, 0, 5) ;
    }

    public function getDay() {
        switch ($this->id_den) {
            case 1:
                return 'Pondelok';
            case 2:
                return 'Utorok';
            case 3:
                return 'Streda';
            case 4:
                return 'Stvrtok';
            case 5:
                return 'Piatok';
        }
    }

    public function getTableCell() {
        return
            '<span class="' . $this->assignCssClasses() . '"
                data-title="' . $this->subject->nazov . '"
                data-type="' . $this->type() . '"
                data-teacher="' . $this->teacher->fullName() . '"
                data-duration="' . $this->duration() . '"
                data-day="' . $this->getDay() . '"
                data-room="' . $this->room->nazov . '"
                data-compulsory="' . $this->subject->povinny .'"
            >
                <span class="title">'. $this->subject->skratka . '</span>
                <span class="room">' . $this->room->nazov . '</span>
                <span class="duration">' . $this->duration(). '</span>
            </span>';
    }

    private function assignCssClasses(){
        $class = "cell ";
        $class .= $this->prednaska == 1 ? "lecture " : "excersise ";
        $class .= $this->subject->povinny == 1 ? "compulsory " : "optional ";

        return $class;
    }
}
