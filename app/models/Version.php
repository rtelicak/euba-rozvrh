<?php

class Version extends \Eloquent {
	protected $fillable = [];

    protected $table = 'verzia';

    protected $primaryKey = 'id_verzia';
}
