<?php namespace Schedule\Repositories;

use \Department;
use \Group;
use \Lesson;
use \Input;

/**
* Search
*/
class SearchRepository {

    public function getDepartmentsByFacultyAndYear($faculty_id, $year_id) {
        return Department::whereIdFakulta($faculty_id)->whereRocnik($year_id)->get()->toArray();
    }

    public function getGroupsByDepartment($department_id) {
        return  Group::whereIdOdbor($department_id)->get()->toArray();
    }


    public function getListOfDepartmentsByFacultyAndYear($faculty_id, $year_id) {
        return $faculty_id && $year_id ? Department::whereIdFakulta($faculty_id)->whereRocnik($year_id)->lists('kod', 'id') : [];
    }

    public function getListOfGroupsByDepartment($department_id) {
        return $department_id ? Group::whereIdOdbor($department_id)->lists('cislo', 'id') : [];
    }

    public function getLessons($group) {
        if (! $group) return [];

        // $l = Lesson::with('subject', 'hour', 'teacher', 'room')
        //     ->whereIdKruzok($group)
        //     ->orderBy('id_den')
        //     ->orderBy('id_hodina')
        //     ->first();

        // echo "<pre>";
        // dd($l->teacher->fullName());

        return Lesson::with('subject', 'hour', 'teacher', 'room')
            ->whereIdKruzok($group)
            ->orderBy('id_den')
            ->orderBy('id_hodina')
            ->get();
    }

}
