<?php namespace Schedule\Repositories;

use Carbon\Carbon;
use Schedule\Localizer;

/**
* Generating schedule table markup
*/
class PresenterRepository {

    protected $lessons;

    function __construct($lessons) {
        $this->lessons = $lessons;
    }

    /*
    |--------------------------------------------------------------------------
    | Make table containing schedule
    |--------------------------------------------------------------------------
    */

    public function getTableMarkup() {
        $html = $this->tableHeader();
        $html .= $this->getDayRows($this->lessons);
        $html .= $this->closeTable();

        return $html;
    }

    private function getDayRows(){
        $html = '';

        // We have 5 days in schedule
        for ($day = 1; $day < 6; $day++) {
            $html .= $this->getDayRow($day);
        }

        return $html;
    }

    // Get markup for day
    private function getDayRow($day) {
        // Open row and get first cell of row - Day title
        $html = $this->makeDayElement($day);

        // There is maximum of 7 lessons per day
        for ($hour = 1; $hour < 8; $hour++) {
            $html .= $this->getCellByDayAndHour($day, $hour);
        }

        $html .= $this->closeRow();

        return $html;
    }

    private function getCellByDayAndHour($day, $hour){
        $lessonsHtml = '';

        foreach ($this->lessons as $lesson) {
            if ($lesson->isItNow($day, $hour)) {
                $lessonsHtml .= $lesson->getTableCell();
            }
        }

        return  strlen($lessonsHtml) ?
            $this->getLessonElement($lessonsHtml) :
            $this->getEmptyTableCell();
    }

    private function getLessonElement($lessonsHtml){
        // if there are multiple lessons in one cell, set special css class
        return strpos($lessonsHtml, '</span><span') ?
            $this->openSpecialCell() . $lessonsHtml . $this->closeCell() :
            $this->openCell() . $lessonsHtml . $this->closeCell();
    }

    private function getEmptyTableCell(){

        return $this->openCell().'<span class="empty">&nbsp;</span>'.$this->closeCell();
    }



    private function makeDayElement($dayIndex){
        $date = Carbon::now('Europe/Bratislava')->startOfWeek()->addDays($dayIndex -1);
        $currentDayClass = $date->isToday() ? 'currentDay' : '';

        return '<tr class="'. $currentDayClass .'"><td class="dayCell">
            <span class="dayTitle">'. Localizer::getSlovakDay($dayIndex) .'</span>
            <span class="dayTitleMobile">'. mb_substr(Localizer::getSlovakDay($dayIndex), 0, 1, 'UTF-8')  .'</span>
            <span class="dayDate">' .$date->day. '. ' . $date->month. '.</span></td>';
    }

    private function tableHeader(){
        $html = '<table><thead><tr>';
        $html .= '<th></th>';
        $html .= '<th>7<sup>30</sup></th>';
        $html .= '<th>9<sup>15</sup></th>';
        $html .= '<th>11<sup>00</sup></th>';
        $html .= '<th>13<sup>30</sup></th>';
        $html .= '<th>15<sup>15</sup></th>';
        $html .= '<th>17<sup>00</sup></th>';
        $html .= '<th>18<sup>35</sup></th>';
        $html .= '</tr></thead>';

        return $html;
    }

    private function closeTable(){
        return '</tbody></table>';
    }

    private function closeRow(){
        return '</tr>';
    }

    private function openCell(){
        return '<td>';
    }

    private function openSpecialCell(){
        return '<td class="multipleLessons">';
    }

    private function closeCell(){
        return '</td>';
    }

    /*
    |--------------------------------------------------------------------------
    | Next Lesson feature
    |--------------------------------------------------------------------------
    */

    public function getNextLesson() {
        if ( ! count($this->lessons) || $this->lessons->isEmpty()) return [];

        // echo "<pre>";
        // dd($this->lessons->first());

        $now = Carbon::now('Europe/Bratislava');
        // $now = Carbon::now('Europe/Bratislava')->subHours(8)->minute(10);
        // dd($now);
        $dayIndex = $now->dayOfWeek;
        $hourIndex = 0;

        // If last lesson has begun (> 18:35)
        if ($now->gt(Carbon::createFromTime(18, 35, 0, 'Europe/Bratislava'))) {
            $dayIndex++;
            $hourIndex = 1;
        }

        // If today is Saturday or Sunday
        if ($dayIndex == 0 || $dayIndex == 6) {
            $dayIndex = 1;
            $hourIndex = 1;
        }

        if ($hourIndex == 0) {
            $hourIndex = $this->getNextLessonHourIndex($now);
        }

        // var_dump($hourIndex);
        // var_dump($dayIndex);
        $nextLesson = $this->findNexLesson($dayIndex, $hourIndex);

        return  $nextLesson;
    }

    private function findNexLesson($day, $hour){
        // maybe there are multiple lesson at given day/hour
        $lessonsFound = [];

        // We can rely on their ASC order
        foreach ($this->lessons as $lesson) {
            if ($lesson->id_den == $day && $lesson->id_hodina >= $hour || $lesson->id_den > $day) {
                $nextLesson = $lesson;
                break;
            }
        }

        // Maybe there are no lessons left in week, so grab first first
        $nextLesson = isset($nextLesson) ? $nextLesson : $this->lessons->first();

        // Posibly there are multiple lessons in such day and hour
        foreach ($this->lessons as $lesson) {
            if ($lesson->isItNow($nextLesson->id_den, $nextLesson->id_hodina)) {
                array_push($lessonsFound, $lesson);
            }
        }

        return $lessonsFound;
    }

    private function incrementDay($day){
        return $day == 5 ? 1 : ++$day;
    }

    private function incrementHour($hour){
        return $hour == 7 ? 1 : ++$hour;
    }

    private function getNextLessonHourIndex($now){
             if ($now->lt(Carbon::createFromTime(7, 30, 0, 'Europe/Bratislava'))) return 1;
        else if ($now->lt(Carbon::createFromTime(9, 15, 0, 'Europe/Bratislava'))) return 2;
        else if ($now->lt(Carbon::createFromTime(11, 00, 0, 'Europe/Bratislava'))) return 3;
        else if ($now->lt(Carbon::createFromTime(13, 30, 0, 'Europe/Bratislava'))) return 4;
        else if ($now->lt(Carbon::createFromTime(15, 15, 0, 'Europe/Bratislava'))) return 5;
        else if ($now->lt(Carbon::createFromTime(17, 00, 0, 'Europe/Bratislava'))) return 6;
        else if ($now->lt(Carbon::createFromTime(18, 35, 0, 'Europe/Bratislava'))) return 7;
    }

    /*
    |--------------------------------------------------------------------------
    | Current lesson feature
    |--------------------------------------------------------------------------
    */

    public function getCurrentLesson() {
        $now = Carbon::now('Europe/Bratislava');

        if ($this->itsNotSchoolTime($now)) {
            return null;
        }

        $hourIndex = $this->getNextLessonHourIndex($now) - 1;

        return  array_first($this->lessons, function($key, $value) use ($now, $hourIndex){
            return $value->id_den == $now->dayOfWeek && $value->id_hodina == $hourIndex;
        });
    }

    private function itsNotSchoolTime($now){
        return
            $now->gt(Carbon::createFromTime(18, 35, 0, 'Europe/Bratislava')) ||
            $now->lt(Carbon::createFromTime(7, 30, 0, 'Europe/Bratislava')) ||
            $now->dayOfWeek == 5 ||
            $now->dayOfWeek == 0;
    }
}
