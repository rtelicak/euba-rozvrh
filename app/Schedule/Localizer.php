<?php  namespace Schedule;

use Carbon\Carbon;

/**
* Localize months, days ...
*/
class Localizer {

    public static function getSlovakMonth($month) {
        switch ($month) {
            case 1:
                return 'Január';
            case 2:
                return 'Február';
            case 3:
                return 'Marec';
            case 4:
                return 'Apríl';
            case 5:
                return 'Máj';
            case 6:
                return 'Jún';
            case 7:
                return 'Júl';
            case 8:
                return 'August';
            case 9:
                return 'September';
            case 10:
                return 'Október';
            case 11:
                return 'November';
            case 12:
                return 'December';
        }
    }

    public static function getSlovakDay($index){
        switch ($index) {
            case 1:
                return 'Pondelok';
            case 2:
                return 'Utorok';
            case 3:
                return 'Streda';
            case 4:
                return 'Štvrtok';
            case 5:
                return 'Piatok';
        }
    }

    public static function getFormattedDate() {
        $dt = Carbon::now('Europe/Bratislava');
        // dd(self::getSlo'vakDay($dt->day);
        // dd(self::getSlovakDay($dt->day));
        $minute = strlen($dt->minute) == 1 ? '0' . $dt->minute : $dt->minute;

        return $dt->hour .':'. $minute .' | '. self::getSlovakDay($dt->dayOfWeek) .' '. $dt->day .'.'.$dt->month .'.'. $dt->year;
    }
}
