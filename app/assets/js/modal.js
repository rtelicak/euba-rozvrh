(function($){

    $(".cell").click(function(){
        var itemAttributes = this.dataset;
        // console.log(itemAttributes);
        var modalElm = $('#detail-modal');

        $('.modal-title').html(itemAttributes.title);
        modalElm.find('.teacher p').html(itemAttributes.teacher);
        modalElm.find('.type p').html(itemAttributes.compulsory == 0 ? "Nepovinný" : "Povinný");
        modalElm.find('.kind p').html(itemAttributes.type);
        modalElm.find('.room p').html(itemAttributes.room);
        modalElm.find('.day p').html(itemAttributes.day);
        modalElm.find('.duration p').html(itemAttributes.duration);

        modalElm.modal("show");
    });

})(jQuery);


