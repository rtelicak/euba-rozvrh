(function(){

    var selectView = Backbone.View.extend({
        el: '',

        initialize: function(args) {
            this.slave = args.slave;
        },

        events: {
            'change': 'updateSlave'
        },

        updateSlave: function() {
            if (this.slave) this.slave.updateData();
        },

        updateData: function(){
            // reset slaves recursively
            if (this.slave) this.slave.reset();

            // don't update data on faculty, those are static
            if (this.el.id == 'faculty') {
                this.selectFirstOption();

                return;
            }

            console.log('Updating data: ' + this.el.id);

            // send get request to populate options
            $.ajax({
                url: 'svc/' + this.el.id,
                method: 'get',
                context: this,
                data: {
                    'year_id': $('#year').val(),
                    'faculty_id': $('#faculty').val(),
                    'department_id': $('#department').val()
            }})
            .done(function(data){
                this.updateOptions(data);
            })
            .fail(function(e){
                return e;
            });
        },

        // set new options from request result data
        updateOptions: function(data){
            this.removeOptions();

            var html = '';
            _.each(data, function(item) {
                html += this.makeOption(item);
            }, this);

            this.$el.append(html);
        },

        // build markup for option
        makeOption: function(item) {
            var title = this.el.id == 'group' ? 'cislo' : 'kod';

            return '<option value="' + item.id + '">' + item[title] + '</option>';
        },

        // select dummy option and remove others
        reset: function() {
            if (this.el.id == 'faculties') {
                this.selectFirstOption();

                return;
            }

            console.log('Reset: ' + this.el.id);
            this.removeOptions();
            if (this.slave) this.slave.reset();
        },

        // remove all options from select
        removeOptions: function() {
            this.selectFirstOption();
            this.$('option').map(function() {
                if (this.value) $(this).remove();
            });
        },

        // select dummy option 'Select @entity'
        selectFirstOption: function(){
            $('#' + this.el.id + '').val($('#' + this.el.id +  'option:first').val());
        }
    });

    var group = new selectView({el: '#group'});
    var department = new selectView({el: '#department', slave: group});
    var faculty = new selectView({el: '#faculty', slave: department});
    var year = new selectView({el: '#year', slave: faculty});

})();
